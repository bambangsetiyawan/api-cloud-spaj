'use strict';

const sql = require('mssql');

const dbibonline = {
    user:"bams",
	password:"12345678",
	server:"DESKTOP-1IO7HHA\\SQLEXPRESS",
	database:"DBIBONLINE",
	connectionTimeout : 300000,
	requestTimeout :300000,
	port:1433
};

const dbworkflow = {
    user:"sa",
	password:"P@ssw0rd",
	server:"104.250.105.214",
	database:"DBWorkflow",
	requestTimeout: 30000,
	port:1433
};

exports.exec_qry = async (obj) => {
    let connsetting = null;
    let properti = obj.returnProperti;
    switch(obj.conn){
        case 'dbibonline':
            connsetting = dbibonline;
        break;
        // case 'dbworkflow':
        //     connsetting = dbworkflow;
        // break;
    }
    console.log('setting bd', connsetting)
    const pool = await sql.connect(connsetting);
    console.log('connect bd', pool)
    try {
        let result = await pool
            .request()
            .query(obj.qry);
            console.log('deklare result', result)
        let hasil = `{"success": true, "${properti}": ${JSON.stringify((result === undefined)?[]:result)}}`;
        console.log(hasil);
        return JSON.parse(hasil);
    } catch (err) {
        return {success: false, errMsg: err};
    } finally {
        pool.close();
        sql.close();
    }
};

exports.exec_sp = async (obj) => {
    let connsetting = null;
    // console.log('udah masuk exec', connsetting)
    let properti = obj.returnProperti;
    switch(obj.conn){
        case 'dbibonline':
            connsetting = dbibonline;
        break;
        // case 'dbworkflow':
        //     connsetting = dbworkflow;
        // break;
    }
    // console.log('udah rubah conn', connsetting)
    const pool = await sql.connect(connsetting);
    // console.log('konek conn', pool)
    try {
        console.log('udah masuk try')
        const request = new sql.Request(pool)
        console.log(obj)
        obj
            .params
            .forEach(obj_data => {

                request.input(obj_data.param, obj_data.type, obj_data.value)
                console.log('obj_data', obj_data)
            });
        
        let result = await request.execute(obj.sp);
        let hasil = `{"success": true, "${properti}"}`; //: ${JSON.stringify(result[0])}
        console.log('masuk0',hasil);
        return JSON.parse('masuk1',hasil);
    } catch (err) {
        return {success: false, errMsg: err};
    } finally {
        pool.close();
        sql.close();
    }
};

exports.create_conn_trans = async (obj) => {

    try {
        let connsetting = null;
        switch(obj.conn){
            case 'dbibonline':
                connsetting = dbibonline;
            break;
            case 'dbworkflow':
                connsetting = dbworkflow;
            break;
        }
        const pool = await sql.connect(connsetting);
        return pool;
    } catch (e) {
        console.log('hirawan x');
        console.log(e.message);
    }
    
};

exports.exec_qry_trans = async (obj, trans_var) => {
    try {
        let result = await new sql.Request(trans_var).query(obj.qry);
        return result;
    } catch (err) {
        console.log('hirawan y');
        return err;
    }
};

exports.exec_sp1 = async (obj) => {
    let connsetting = dbibonline;
    let properti = obj.returnProperti;
    
    switch(obj.conn){
        case 'dbibonline':
            connsetting = dbibonline;
        break;
    }

    
    const pool1 = await new sql.ConnectionPool(connsetting).connect();
    try {
        console.log('udah masuk try')
        let request = new sql.Request(pool1);
        // console.log(pool1)
        obj
            .params
            .forEach(obj_data => {
                request.input(obj_data.param, obj_data.type, obj_data.value)
            });
        let result = await request.execute(obj.sp)
        console.log('masuk0',result);
        
        if(result){
            
            reshasil ={
                rescode: '00',
                resdata: result.recordset,
                reserror: []
            };
        }else{
            reshasil ={
                rescode: '01',
                resdata: [],
                reserror: [
                    {
                        errcode: '401',
                        errmsg: 'The Web Service is in trouble.',
                    }
                ]
            };
        }
        return reshasil;    
    } catch (err) {
        reshasil ={
            rescode: '01',
            resdata: [],
            reserror: [
                {
                    errcode: '404',
                    errmsg: err.message ? err.message : err,
                }
            ]
        };
        return reshasil;
    }
};

