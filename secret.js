var sqlDB = require("mssql");

module.exports.dbConfig = {
    user: 'sa',
    password: 'P@ssw0rd',
    server: '104.250.105.214', 
    database: 'dbibonline_',
	connectionTimeout : 300000,
	requestTimeout :300000,
	port:1433
};

// module.exports.dbLocalConfig = {
//     "user": 'bams',
//     "password": '12345678',
//     "server": 'DESKTOP-1IO7HHA',
//     "database": 'DBIBONLINE',
//     "port": '1433',
//     "dialect": "mssql",
//     "dialectOptions": {
//         "instanceName": "SQLEXPRESS"
//     }
// };

module.exports.dbLocalConfig = {
    user: 'bams',
    password: '12345678',
    server: 'DESKTOP-1IO7HHA\\SQLEXPRESS', 
    database: 'DBIBONLINE',
	connectionTimeout : 300000,
	requestTimeout :300000,
	port:1433
};