'use strict';
const sql = require('mssql');

const dbibonline = {
    user: 'saAplikasi',
    password: 'P@ssw0rdbjs',
    server: '10.28.0.36',
    database: 'dbibonline',
    connectionTimeout : 300000,
	requestTimeout :300000,
    options: {
        encrypt: false
    }
};

exports.exec_sp = async (obj) => {
    let connsetting = null;
    let properti = obj.returnProperti;
    connsetting = dbibonline;
    // switch(obj.conn){
    //     case 'dbmylife':
    //         connsetting = dbmylife;
    //     break;
    //     case 'dbibonline':
    //         connsetting = dbibonline;
    //     break;
    //     case 'dbworkflow':
    //         connsetting = dbworkflow;
    //     break;
    //     case 'davestera':
    //         connsetting = davestera;
    //     break;
    //     case 'inbranchbjs':
    //         connsetting = inbranchbjs;
    //     break;
    //     case 'satelitebjs':
    //         connsetting = satelitebjs;
    //     break;
    //     case 'dbcif':
    //         connsetting = dbcif;
    //     break;
    // }
    const pool = await sql.connect(connsetting);
    try {
        
        const request = new sql.Request(pool)
        obj
            .params
            .forEach(obj_data => {
                request.input(obj_data.param, obj_data.type, obj_data.value)
            });
        let result = await request.execute(obj.sp);
        let hasil = `{"success": true, "${properti}": ${JSON.stringify(result.recordset)}}`;
        return JSON.parse(hasil);
    } catch (err) {
        return {success: false, errMsg: err};
    } finally {
        pool.close();
        sql.close();
    }
};