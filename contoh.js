
var sql = require("mssql");

exports.getUser = function (req,res) {
        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('SELECT TOP 3 * FROM dbo.nb_spaj_kuesion_medis;', function (err, recordset) {
            
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);
        });
        
        // console.log('sukses');
}


exports.getUserLocal = function (req,res) {
    // create Request object
    var request = new sql.Request();
       
    // query to the database and get the records
    request.query('SELECT TOP 3 * FROM dbo.nb_spaj_kuesion_medis;', function (err, recordset) {
        
        if (err) console.log(err)

        // send records as a response
        res.send(recordset);
        
    });

    
    // console.log('sukses');
}


// exports.loaddpp = async (req, res) => {
//     try {
//         let kode_produk = req.body.kode_produk;
//         let param1 = req.body.param1;
//         let no_registrasi = req.body.no_registrasi;

//         let result = {
//             success: false,
//             carapembayaran: [],
//             periodebayar: [],
//             jeniskelamin: [],
//             status: [],
//             hub: [],
//             manfaat: [],
//             showpp: [],
//             masabayar: []
//         };

//         let objsp = {
//             conn: 'dbibonline',
//             sp: 'dbo.sp_get_reff_acc',
//             returnProperti: 'rows',
//             params: [
//                 {
//                     param: 'reff',
//                     type: sql.VarChar(100),
//                     value: 'carapembayaran'
//                 },
//                 {
//                     param: 'kode_produk',
//                     type: sql.VarChar(100),
//                     value: kode_produk
//                 },
//                 {
//                     param: 'param1',
//                     type: sql.VarChar(100),
//                     value: param1
//                 }
//             ]
//         }

//         let hasil1 = await helper.exec_sp(objsp);
//         result.carapembayaran = hasil1.rows;
//         var param11 = '';
//         if (kode_produk === '02008' || kode_produk === '04008') {
//             param11 = 'DASETERA';
//         } else {
//             param11 = '';
//         }
//         var reff = '';
//         if (kode_produk === 'UL000005' || kode_produk === 'UL000006' || kode_produk === 'UL000008' || kode_produk === 'UL000010' || kode_produk === 'UL000012' || kode_produk === 'UL000014') {
//             reff = 'periodebayarSin';
//         } else {
//             reff = 'periodebayar';
//         }

//         objsp.params[0].value = reff;
//         objsp.params[2].value = param11;

//         let hasil2 = await helper.exec_sp(objsp);
//         result.periodebayar = hasil2.rows;

//         objsp.params[0].value = 'jeniskelamin';
//         objsp.params[2].value = param1;
        
//         let hasil3 = await helper.exec_sp(objsp);
//         result.jeniskelamin = hasil3.rows;

//         objsp.params[0].value='status';
//         let hasil4 = await helper.exec_sp(objsp);
//         result.status = hasil4.rows;

//         objsp.params[0].value='manfaat';
//         let hasil5 = await helper.exec_sp(objsp);
//         result.manfaat = hasil5.rows;

//         objsp.params[0].value='hub';
//         let hasil6 = await helper.exec_sp(objsp);
//         result.hub = hasil6.rows;

//         objsp.qry = `select a.* from nb_nasabah_byrpremi a left join nb_spaj b on a.id_aplikasi=b.id_aplikasi
//         where b.TransNo='${no_registrasi}'`;
//         let hasil7 = await helper.exec_qry(objsp);
//         result.showpp = hasil7.rows[0];

//         objsp.params[0].value='masabayar';
//         objsp.params[2].value='';
//         let hasil8 = await helper.exec_sp(objsp);
//         result.masabayar = hasil8.rows;
//         result.success = true;

//         res.json(result);

//     } catch (err) {
//         res.send(err);
//     }
// };


insertklaim: async (req,res) => {
    let reshasil ={
        rescode: '01',
        resdata: [],
        reserror: []
    };
    try {
            
            var dacol_klaim_detail = new sql.Table();
            dacol_klaim_detail.columns.add('ms_list_product_class', sql.Int);
            // var ms_list_product_class = req.body.ms_list_product_class;
            // var ms_list_product_class_arr = ms_list_product_class.split(",");
            // if (ms_list_product_class_arr.length === 0){
            //     let ress = {
            //         status : 'error',
            //         error : 'list produk class kosong !.',
            //     }
            //     res.send(ress)
            //     return;
            // }

            // ms_list_product_class_arr.forEach(produk_class => {
            //     dacol_klaim_detail.rows.add(produk_class);
            // });


            var ms_list_product_class = req.body.ms_list_product_class ? req.body.ms_list_product_class : [];
            if (ms_list_product_class.length > 0){
                for(var a in ms_list_product_class){
                    dacol_klaim_detail.rows.add(ms_list_product_class[a]);
                }
            }else{
                let ress = {
                    status : 'error',
                    error : 'list produk class kosong !.',
                }
                res.send(ress)
                return;
            }

            var dacol_klaim_urifile = new sql.Table();
            dacol_klaim_urifile.columns.add('nama_file', sql.VarChar(150));
            dacol_klaim_urifile.columns.add('content_file', sql.VarChar(50));
            dacol_klaim_urifile.columns.add('length_file', sql.Int);
            dacol_klaim_urifile.columns.add('uri_file', sql.VarChar(255));

            var files_ = req.body.files;

            let curr_path ='';
            let newUri ='';
            
            files_.forEach((data) => {
                
                curr_path = `/srv/apimylife/public/klaim/${req.body.no_polis}_${data.nama_file}.${data.content_file}`; 
                fs.writeFile(curr_path, data.base64, 'base64', function(err) {
                    console.log(err);
                });
            
                newUri = `http://www.brilife.co.id/mylifeapi/static/klaim/${req.body.no_polis}_${data.nama_file}.${data.content_file}`;
                dacol_klaim_urifile.rows.add(data.nama_file,data.content_file,data.length_file,newUri);
            });

            let objsp = {
                conn: 'cam',
                sp: 'dbo.qsp_klaim',
                returnProperti: 'data',
                params: [
                    {
                        param: 'no_polis',
                        type: sql.VarChar(50),
                        value: req.body.no_polis
                    },
                    {
                        param: 'id_user',
                        type: sql.VarChar(50),
                        value: req.body.id_user
                    },
                    {
                        param: 'nik',
                        type: sql.VarChar(50),
                        value: req.body.nik
                    },
                    {
                        param: 'nama',
                        type: sql.VarChar(50),
                        value: req.body.nama
                    },
                    {
                        param: 'ms_status_hubungan',
                        type: sql.Int,
                        value: req.body.ms_status_hubungan
                    },
                    {
                        param: 'dacol_klaim_detail',
                        type: sql.TVP,
                        value: dacol_klaim_detail
                    },
                    {
                        param: 'dacol_klaim_urifile',
                        type: sql.TVP,
                        value: dacol_klaim_urifile
                    }
                ]
            }

            var hasil_cp = await helpers.exec_sp(objsp);

            reshasil=hasil_cp;
            if(reshasil.rescode == '00'){
                if(reshasil.resdata.length > 0){
                    let ress = {
                        success: 'ok',
                        data:  reshasil.resdata
                    }
                    
                    res.send(ress);
                }else{
                    let ress = {
                        status : 'error',
                        error : 'Data tidak ditemukan',
                    }
                    res.send(ress);
                }
            }else{
                let ress = {
                    status : 'error',
                    error : reshasil.reserror? reshasil.reserror[0].errmsg : '',
                }

                res.send(ress);
            }
    } catch (err) {
        let ress = {
            status : 'error',
            error : err ? (err.message ? err.message : '') : '',
        }
        res.send(ress)
        return;
    }
    
}