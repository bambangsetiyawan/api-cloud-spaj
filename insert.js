'use strict';

// var helper = require('../helpers/global');
const sql = require('mssql');

exports.insertTransaksi = async (req,res) => {
        let reshasil ={
            rescode: '01',
            resdata: [],
            reserror: []
        };
        try {

            //nb_spaj
            var nb_spaj = req.body.data.nb_spaj;
            var tbl_nbspaj = new sql.Table();
            tbl_nbspaj.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nbspaj.columns.add('TransNo'. sql.NVarChar(18));
            tbl_nbspaj.columns.add('id_ilustrasi'. sql.NVarChar(18));
            tbl_nbspaj.columns.add('id_pemegang_polis'. sql.Numeric(18));
            tbl_nbspaj.columns.add('id_tertanggung'. sql.Numeric(18));
            tbl_nbspaj.columns.add('id_pasangan'. sql.Numeric(18));
            tbl_nbspaj.columns.add('usia_pemegang_polis'. sql.Int);
            tbl_nbspaj.columns.add('usia_pl_th'. sql.Int);
            tbl_nbspaj.columns.add('usia_pl_bl'. sql.Int);
            tbl_nbspaj.columns.add('usia_pl_dy'. sql.Int);
            tbl_nbspaj.columns.add('usia_tertanggung'. sql.Int);
            tbl_nbspaj.columns.add('usia_tt_th'. sql.Int);
            tbl_nbspaj.columns.add('usia_tt_bl'. sql.Int);
            tbl_nbspaj.columns.add('usia_tt_dy'. sql.Int);
            tbl_nbspaj.columns.add('usia_psg_pemegang_polis'. sql.Int);
            tbl_nbspaj.columns.add('usia_ppp_th'. sql.Int);
            tbl_nbspaj.columns.add('usia_ppp_bl'. sql.Int);
            tbl_nbspaj.columns.add('usia_ppp_dy'. sql.Int);
            tbl_nbspaj.columns.add('id_hubungan_tt'. sql.Int);
            tbl_nbspaj.columns.add('hubungan_lainnya'. sql.VarChar(50));
            tbl_nbspaj.columns.add('latitude'. sql.VarChar(250));
            tbl_nbspaj.columns.add('longitude'. sql.VarChar(250));
            tbl_nbspaj.columns.add('address'. sql.VarChar(200));
            tbl_nbspaj.columns.add('tgl_complete'. sql.SmallDateTime);
            tbl_nbspaj.columns.add('tgl_apply_dana'. sql.SmallDateTime);
            tbl_nbspaj.columns.add('CIF_BRI'. sql.VarChar(50));
            tbl_nbspaj.columns.add('cif_pp_new'. sql.Int);
            tbl_nbspaj.columns.add('cif_tt_new'. sql.Int);
            tbl_nbspaj.columns.add('cif_ppp_new'. sql.Int);
            tbl_nbspaj.columns.add('id_agent'. sql.VarChar(50));
            tbl_nbspaj.columns.add('id_branch'. sql.VarChar(4));
            tbl_nbspaj.columns.add('Id_supervisor'. sql.VarChar(50));
            tbl_nbspaj.columns.add('no_license'. sql.VarChar(50));
            tbl_nbspaj.columns.add('no_briva'. sql.VarChar(18));
            tbl_nbspaj.columns.add('id_polis_ke_bro'. sql.Int);
            tbl_nbspaj.columns.add('catatan_extra_premi'. sql.VarChar);
            tbl_nbspaj.columns.add('catatan_exclusion'. sql.VarChar);
            
            //nb_spaj_agen    
            var nb_spaj_agen = req.body.data.nb_spaj_agen;
            var tbl_nb_spaj_agen = new sql.Table();
            tbl_nb_spaj_agen.columns.add('sequenceno'. sql.Int);
            tbl_nb_spaj_agen.columns.add('TransNo'. sql.NVarChar(50));
            tbl_nb_spaj_agen.columns.add('no_agen'. sql.VarChar(25));
            tbl_nb_spaj_agen.columns.add('no_agen_induk'. sql.VarChar(25));
            tbl_nb_spaj_agen.columns.add('kd_jabatan'. sql.VarChar(50));
            tbl_nb_spaj_agen.columns.add('kd_jabtan_induk'. sql.VarChar(50));

            //dbo.nb_spaj_biller
            var nb_spaj_biller = req.body.data.nb_spaj_biller;
            var tbl_nb_spaj_biller = new sql.Table();
            tbl_nb_spaj_biller.columns.add('id'. sql.Numeric(18));
            tbl_nb_spaj_biller.columns.add('tgl_generate'. sql.DateTime);
            tbl_nb_spaj_biller.columns.add('no_spaj'. sql.VarChar(25));
            tbl_nb_spaj_biller.columns.add('no_rekening'. sql.VarChar(100));
            tbl_nb_spaj_biller.columns.add('remarks'. sql.VarChar(100));
            tbl_nb_spaj_biller.columns.add('amount_pelunasan'. sql.Decimal(18));
            tbl_nb_spaj_biller.columns.add('periode_bayar'. sql.Int);
            tbl_nb_spaj_biller.columns.add('jumlah_data'. sql.Int);
            tbl_nb_spaj_biller.columns.add('no'. sql.Int);
            tbl_nb_spaj_biller.columns.add('kd_bank'. sql.VarChar(500));
            tbl_nb_spaj_biller.columns.add('nama_pemilik_rekening'. sql.VarChar(100));
            tbl_nb_spaj_biller.columns.add('keterangan_status'. sql.VarChar(100));
            tbl_nb_spaj_biller.columns.add('st_process'. sql.Int);
            
            //dbo.nb_spaj_biller_proses
            var nb_spaj_biller_proses = req.body.data.nb_spaj_biller_proses;
            var tbl_nb_spaj_biller_proses = new sql.Table();
            tbl_nb_spaj_biller_proses.columns.add('id'. sql.Int);
            tbl_nb_spaj_biller_proses.columns.add('waktu_process'. sql.Date);
            tbl_nb_spaj_biller_proses.columns.add('status_aktif'. sql.Int);
            tbl_nb_spaj_biller_proses.columns.add('status_read'. sql.VarChar(200));

            //dbo.nb_spaj_file_biller
            var nb_spaj_file_biller = req.body.data.nb_spaj_file_biller;
            var tbl_nb_spaj_file_biller = new sql.Table();
            tbl_nb_spaj_file_biller.columns.add('id'. sql.Numeric(18));
            tbl_nb_spaj_file_biller.columns.add('tgl_file'. sql.DateTime);
            tbl_nb_spaj_file_biller.columns.add('nama_file_tagihan'. sql.VarChar(225));
            tbl_nb_spaj_file_biller.columns.add('nama_file_tagihan_sukses'. sql.VarChar(255));
            tbl_nb_spaj_file_biller.columns.add('nama_file_tagihan_gagal'. sql.VarChar(225));
            tbl_nb_spaj_file_biller.columns.add('kd_bank'. sql.VarChar(50));
            tbl_nb_spaj_file_biller.columns.add('tgl_pelunasan'. sql.DateTime);
            tbl_nb_spaj_file_biller.columns.add('amount_tagihan'. sql.Decimal(18));
            tbl_nb_spaj_file_biller.columns.add('amount_pelunasan'. sql.Decimal(18));
            tbl_nb_spaj_file_biller.columns.add('periode'. sql.Int);
            tbl_nb_spaj_file_biller.columns.add('tgl_generate'. sql.Date);
            tbl_nb_spaj_file_biller.columns.add('no_awal'. sql.Int);
            tbl_nb_spaj_file_biller.columns.add('no_akhir'. sql.Int);
            
            //dbo.nb_spaj_integral
            var nb_spaj_integral = req.body.data.nb_spaj_integral;
            var tbl_nb_spaj_integral = new sql.Table();
            tbl_nb_spaj_integral.columns.add('TransNo'. sql.VarChar(50));
            tbl_nb_spaj_integral.columns.add('ILContractNumber'. sql.VarChar(50));
            tbl_nb_spaj_integral.columns.add('UDWStatus'. sql.VarChar(50));
            tbl_nb_spaj_integral.columns.add('date'. sql.DateTime);

            //dbo.nb_spaj_integral_hist
            var nb_spaj_integral_hist = req.body.data.nb_spaj_integral_hist;
            var tbl_nb_spaj_integral_hist = new sql.Table();
            tbl_nb_spaj_integral_hist.columns.add('TransNo'. sql.VarChar(50));
            tbl_nb_spaj_integral_hist.columns.add('ILContractNumber'. sql.VarChar(50));
            tbl_nb_spaj_integral_hist.columns.add('date_log'. sql.DateTime);

            //dbo.nb_spaj_attach
            var nb_spaj_attach = req.body.data.nb_spaj_attach;
            var tbl_nb_spaj_attach = new sql.Table();
            tbl_nb_spaj_attach.columns.add('id'. sql.Int);
            tbl_nb_spaj_attach.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_attach.columns.add('id_tipe_file'. sql.Int);
            tbl_nb_spaj_attach.columns.add('keterangan_file'. sql.NVarChar);
            tbl_nb_spaj_attach.columns.add('nama_file'. sql.NVarChar);
            tbl_nb_spaj_attach.columns.add('content_file'. sql.VarChar(50));
            tbl_nb_spaj_attach.columns.add('length_file'. sql.Int);
            tbl_nb_spaj_attach.columns.add('attach_file'. sql.VarBinary);
            tbl_nb_spaj_attach.columns.add('attach_gambar'. sql.Text);
            tbl_nb_spaj_attach.columns.add('nama_jelas'. sql.VarChar(50));
            tbl_nb_spaj_attach.columns.add('user_entry'. sql.VarChar(50));
            tbl_nb_spaj_attach.columns.add('date_entry'. sql.DateTime);
            tbl_nb_spaj_attach.columns.add('user_update'. sql.VarChar(50));
            tbl_nb_spaj_attach.columns.add('date_update'. sql.DateTime);
            tbl_nb_spaj_attach.columns.add('is_underwriter'. sql.TinyInt);
            
            //dbo.nb_spaj_history_underwriter
            var nb_spaj_history_underwriter = req.body.data.nb_spaj_history_underwriter;
            var tbl_nb_spaj_history_underwriter = new sql.Table();
            tbl_nb_spaj_history_underwriter.columns.add('id'. sql.Int);
            tbl_nb_spaj_history_underwriter.columns.add('Transno'. sql.VarChar(20));
            tbl_nb_spaj_history_underwriter.columns.add('status_udw'. sql.VarChar(50));
            tbl_nb_spaj_history_underwriter.columns.add('ket_udw'. sql.VarChar);
            tbl_nb_spaj_history_underwriter.columns.add('user_record'. sql.VarChar(50));
            tbl_nb_spaj_history_underwriter.columns.add('tgl_record'. sql.DateTime);
            tbl_nb_spaj_history_underwriter.columns.add('user_update'. sql.VarChar(50));
            tbl_nb_spaj_history_underwriter.columns.add('tgl_update'. sql.DateTime);
            
            //dbo.nb_spaj_kuesion_medis
            var nb_spaj_kuesion_medis = req.body.data.nb_spaj_kuesion_medis;
            var tbl_nb_spaj_kuesion_medis = new sql.Table();
            tbl_nb_spaj_kuesion_medis.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_kuesion_medis.columns.add('kesehatanTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('kesehatanTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('hobiTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('hobiTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('perokokTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('JumlahBatangTT'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('TelahBerjalanRokok'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('tinggibadanTT'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('beratbadanTT'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('bmiTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('bmiBear'. sql.Float);
            tbl_nb_spaj_kuesion_medis.columns.add('bmiCWine'. sql.Float);
            tbl_nb_spaj_kuesion_medis.columns.add('bmiWhisky'. sql.Float);
            tbl_nb_spaj_kuesion_medis.columns.add('kebiasaanTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('ketkebiasaanTT'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('obatobatanTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('kesehatanTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('JenisObat'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('JumlahObat'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('MataTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('MataTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('THTTT'. sql.VarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('THTTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('ParuParuTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('ParuParuTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('JantungTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('JantungTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('OrganPerutTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('OrganPerutTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemKemihKelaminTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemKemihKelaminTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemSyarafKerangkaOtotTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemSyarafKerangkaOtotTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('TulangKulitTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('TulangKulitTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemKelenjarDarahTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemKelenjarDarahTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemKekebalanInveksiTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('SistemKekebalanInveksiTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('TumorTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('TumorTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('MenderitaLainnyaTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('MenderitaLainnyaTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('RawatInapTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('RawatInapTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('PemeriksaanLabTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('PemeriksaanLabTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('PengobatanJiwaTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('PengobatanJiwaTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('transfusiTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('transfusiTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('donorTT'. sql.VarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('donorTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('hamilTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('hamilTTKet'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('komplikasihamilTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('komplikasihamilTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('keluhanTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('keluhanTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('kondisilahirTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('kondisilahirTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('lamahamilTT'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('namadokteranakTT'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('alamatdokteranakTT'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('panjanganakTT'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('beratanakTT'. sql.Int);
            tbl_nb_spaj_kuesion_medis.columns.add('keluargaJantungTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('keluargaJantungTTKet'. sql.NVarChar(250));
            tbl_nb_spaj_kuesion_medis.columns.add('penyakitdaseTT'. sql.NVarChar(10));
            tbl_nb_spaj_kuesion_medis.columns.add('penyakitdaseTTKet'. sql.NVarChar(250));

            //dbo.nb_spaj_master
            var nb_spaj_master = req.body.data.nb_spaj_master;
            var tbl_nb_spaj_master = new sql.Table();
            tbl_nb_spaj_master.columns.add('id'. sql.Int);
            tbl_nb_spaj_master.columns.add('id_cat'. sql.VarChar(50));
            tbl_nb_spaj_master.columns.add('nama_cat'. sql.VarChar(50));
            tbl_nb_spaj_master.columns.add('deskripsi'. sql.VarChar(100));
            tbl_nb_spaj_master.columns.add('value'. sql.VarChar(100));
            tbl_nb_spaj_master.columns.add('current_value'. sql.VarChar(100));
            
            //dbo.nb_spaj_master_map$
            var nb_spaj_master_map$ = req.body.data.nb_spaj_master_map$;
            var tbl_nb_spaj_master_map$ = new sql.Table();
            tbl_nb_spaj_master$.columns.add('no'. sql.BigInt);
            tbl_nb_spaj_master$.columns.add('master_of_product'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('company'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('key_size'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('table_name'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('table_detail'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('value'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('value2'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('current_value'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('current_value_desc'. sql.NVarChar);
            tbl_nb_spaj_master$.columns.add('svr_db_tbl_field'. sql.NVarChar);

            //dbo.nb_spaj_pemilikanasr
            var nb_spaj_pemilikanasr = req.body.data.nb_spaj_pemilikanasr;
            var tbl_nb_spaj_pemilikanasr = new sql.Table();
            tbl_nb_spaj_pemilikanasr.columns.add('id_pemilikanasr'. sql.Numeric(18));
            tbl_nb_spaj_pemilikanasr.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_pemilikanasr.columns.add('asuransi1'. sql.NVarChar(10));
            tbl_nb_spaj_pemilikanasr.columns.add('jawabket1'. sql.NVarChar(50));
            tbl_nb_spaj_pemilikanasr.columns.add('asuransi2'. sql.NVarChar(10));
            tbl_nb_spaj_pemilikanasr.columns.add('asuransi3'. sql.NVarChar(10));

            //dbo.nb_spaj_pemilikanasr_ket_2
            var nb_spaj_pemilikanasr_ket_2 = req.body.data.nb_spaj_pemilikanasr_ket_2;
            var tbl_nb_spaj_pemilikanasr_ket_2 = new sql.Table();
            tbl_nb_spaj_pemilikanasr.columns.add('id_keterangan_2_ref'. sql.Numeric(18));
            tbl_nb_spaj_pemilikanasr.columns.add('id_pemilikanasr'. sql.Numeric(18));
            tbl_nb_spaj_pemilikanasr.columns.add('polis_pemilik'. sql.NVarChar(50));
            tbl_nb_spaj_pemilikanasr.columns.add('uang_pertanggungan'. sql.Float);
            tbl_nb_spaj_pemilikanasr.columns.add('tanggal_aktif'. sql.Date);
            tbl_nb_spaj_pemilikanasr.columns.add('tanggal_akhir'. sql.Date);
            tbl_nb_spaj_pemilikanasr.columns.add('tanggal_batal'. sql.Date);
            
            //dbo.nb_spaj_pemilikanasr_ket_3
            var nb_spaj_pemilikanasr_ket_3 = req.body.data.nb_spaj_pemilikanasr_ket_3;
            var tbl_nb_spaj_pemilikanasr_ket_3 = new sql.Table();
            tbl_nb_spaj_pemilikanasr_ket_3.columns.add('id_keterangan_3_ref'. sql.Numeric(18));
            tbl_nb_spaj_pemilikanasr_ket_3.columns.add('id_pemilikanasr'. sql.Numeric(18));
            tbl_nb_spaj_pemilikanasr_ket_3.columns.add('nama_perusahaan'. sql.NVarChar(50));
            tbl_nb_spaj_pemilikanasr_ket_3.columns.add('mulai_berlaku'. sql.Date);
            tbl_nb_spaj_pemilikanasr_ket_3.columns.add('uang_pertanggungan'. sql.Float);

            //dbo.nb_spaj_pert_investasi
            var nb_spaj_pert_investasi = req.body.data.nb_spaj_pert_investasi;
            var tbl_nb_spaj_pert_investasi = new sql.Table();
            tbl_nb_spaj_pert_investasi.columns.add('id_pert_investasi'. sql.Numeric(18));
            tbl_nb_spaj_pert_investasi.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_pert_investasi.columns.add('kode_produk'. sql.VarChar(50));
            tbl_nb_spaj_pert_investasi.columns.add('jup'. sql.Float);
            tbl_nb_spaj_pert_investasi.columns.add('premidasar'. sql.Float);
            tbl_nb_spaj_pert_investasi.columns.add('premitopup'. sql.Float);
            tbl_nb_spaj_pert_investasi.columns.add('p_topup_tunggal'. sql.TinyInt);
            tbl_nb_spaj_pert_investasi.columns.add('topup_tunggal'. sql.Float);
            tbl_nb_spaj_pert_investasi.columns.add('masa_th'. sql.SmallInt);
            tbl_nb_spaj_pert_investasi.columns.add('biaya_asuransi'. sql.Float);
            tbl_nb_spaj_pert_investasi.columns.add('em_persen'. sql.Decimal(18));
            tbl_nb_spaj_pert_investasi.columns.add('em_permil'. sql.Decimal(18));
            tbl_nb_spaj_pert_investasi.columns.add('extra_premi'. sql.Float);
            tbl_nb_spaj_pert_investasi.columns.add('no_agenda_ep'. sql.VarChar(500));

            //dbo.nb_spaj_profil_resiko
            var nb_spaj_profil_resiko = req.body.data.nb_spaj_profil_resiko;
            var tbl_nb_spaj_profil_resiko = new sql.Table();
            tbl_nb_spaj_profil_resiko.columns.add('id'. sql.Int);
            tbl_nb_spaj_profil_resiko.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_profil_resiko.columns.add('no_pertanyaan'. sql.Int);
            tbl_nb_spaj_profil_resiko.columns.add('nilai'. sql.Numeric(18));
            tbl_nb_spaj_profil_resiko.columns.add('user_entry'. sql.VarChar(50));
            tbl_nb_spaj_profil_resiko.columns.add('date_entry'. sql.DateTime);
            tbl_nb_spaj_profil_resiko.columns.add('user_update'. sql.VarChar(50));
            tbl_nb_spaj_profil_resiko.columns.add('date_update'. sql.DateTime);
            
            //dbo.nb_spaj_refferal
            var nb_spaj_refferal = req.body.data.nb_spaj_refferal;
            var tbl_nb_spaj_refferal = new sql.Table();
            tbl_nb_spaj_refferal.columns.add('id'. sql.Int);
            tbl_nb_spaj_refferal.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_refferal.columns.add('idbranch'. sql.Int);
            tbl_nb_spaj_refferal.columns.add('kode_produk'. sql.VarChar(20));
            tbl_nb_spaj_refferal.columns.add('kode_regional'. sql.VarChar(2));
            tbl_nb_spaj_refferal.columns.add('kanca_kcp_unit'. sql.VarChar(4));
            tbl_nb_spaj_refferal.columns.add('kode_uker'. sql.VarChar(4));
            tbl_nb_spaj_refferal.columns.add('nama_refferal'. sql.VarChar(50));
            tbl_nb_spaj_refferal.columns.add('personal_number'. sql.VarChar(50));
            tbl_nb_spaj_refferal.columns.add('handphone'. sql.VarChar(50));
            tbl_nb_spaj_refferal.columns.add('ispriority'. sql.TinyInt);
            tbl_nb_spaj_refferal.columns.add('fisikBancas'. sql.TinyInt);
            tbl_nb_spaj_refferal.columns.add('extravaganza'. sql.Int);
            tbl_nb_spaj_refferal.columns.add('issalesgenerated'. sql.TinyInt);

            //dbo.nb_spaj_row_invest_pert_investasi
            var nb_spaj_row_invest_pert_investasi = req.body.data.nb_spaj_row_invest_pert_investasi;
            var tbl_nb_spaj_row_invest_pert_investasi = new sql.Table();
            tbl_nb_spaj_row_invest_pert_investasi.columns.add('id_row_invest_pert_investasi'. sql.Int);
            tbl_nb_spaj_row_invest_pert_investasi.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_row_invest_pert_investasi.columns.add('id_pert_investasi'. sql.Numeric(18));
            tbl_nb_spaj_row_invest_pert_investasi.columns.add('id_investasi'. sql.Int);
            tbl_nb_spaj_row_invest_pert_investasi.columns.add('jenis_dana_investasi'. sql.VarChar(80));
            tbl_nb_spaj_row_invest_pert_investasi.columns.add('alokasi_investasi'. sql.Int);

            //dbo.nb_spaj_row_rider_pert_investasi
            var nb_spaj_row_rider_pert_investasi = req.body.data.nb_spaj_row_rider_pert_investasi;
            var tbl_nb_spaj_row_rider_pert_investasi = new sql.Table();
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('id_row_rider_pert_investasi'. sql.Numeric(18));
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('id_pert_investasi'. sql.Numeric(18));
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('id_manfaat_rider'. sql.NVarChar(50));
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('manfaat_rider'. sql.NVarChar(80));
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('uprider'. sql.Float);
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('masa_th'. sql.SmallInt);
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('biaya_asuransi'. sql.Float);
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('em_persen'. sql.Decimal(18));
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('em_persen'. sql.Decimal(18));
            tbl_nb_spaj_row_rider_pert_investasi.columns.add('extra_premi'. sql.Float);

            //dbo.nb_spaj_setup
            var nb_spaj_setup = req.body.data.nb_spaj_setup;
            var tbl_nb_spaj_setup = new sql.Table();
            tbl_nb_spaj_setup.columns.add('id'. sql.Int);
            tbl_nb_spaj_setup.columns.add('nama_setup'. sql.VarChar(100));
            tbl_nb_spaj_setup.columns.add('nama_proses'. sql.VarChar(50));
            tbl_nb_spaj_setup.columns.add('kode_produk'. sql.VarChar(50));
            tbl_nb_spaj_setup.columns.add('value'. sql.VarChar(50));
            tbl_nb_spaj_setup.columns.add('aktif'. sql.bit);
            tbl_nb_spaj_setup.columns.add('tgl_record'. sql.DateTime);

            //dbo.nb_spaj_skpr
            var nb_spaj_skpr = req.body.data.nb_spaj_skpr;
            var tbl_nb_spaj_skpr = new sql.Table();
            tbl_nb_spaj_skpr.columns.add('id'. sql.Int);
            tbl_nb_spaj_skpr.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_skpr.columns.add('nama_bank'. sql.VarChar(100));
            tbl_nb_spaj_skpr.columns.add('nomor_rekening'. sql.VarChar(100));
            tbl_nb_spaj_skpr.columns.add('nama_pemilik_rekening'. sql.VarChar(50));
            tbl_nb_spaj_skpr.columns.add('jenis_identitas'. sql.VarChar(25));
            tbl_nb_spaj_skpr.columns.add('no_identitas'. sql.VarChar(30));
            tbl_nb_spaj_skpr.columns.add('no_hp'. sql.VarChar(20));
            tbl_nb_spaj_skpr.columns.add('email'. sql.VarChar(50));
            tbl_nb_spaj_skpr.columns.add('id_hub_pr_pp'. sql.VarChar(50));
            tbl_nb_spaj_skpr.columns.add('hub_pr_pp_ket'. sql.NVarChar);
            tbl_nb_spaj_skpr.columns.add('tgl_pendebetan'. sql.Int);
            tbl_nb_spaj_skpr.columns.add('nomor_rekening'. sql.VarChar(100));
            tbl_nb_spaj_skpr.columns.add('id_cara_bayar_premi'. sql.VarChar(50));
            tbl_nb_spaj_skpr.columns.add('user_entry'. sql.VarChar(50));
            tbl_nb_spaj_skpr.columns.add('date_entry'. sql.DateTime);
            tbl_nb_spaj_skpr.columns.add('user_update'. sql.VarChar(50));
            tbl_nb_spaj_skpr.columns.add('date_update'. sql.DateTime);
            tbl_nb_spaj_skpr.columns.add('carabayarpp'. sql.Int);

            //dbo.nb_spaj_termaslahat
            var nb_spaj_termaslahat = req.body.data.nb_spaj_termaslahat;
            var tbl_nb_spaj_termaslahat = new sql.Table();
            tbl_nb_spaj_skpr.columns.add('id_termaslahat'. sql.Numeric(18));
            tbl_nb_spaj_skpr.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_spaj_skpr.columns.add('nama'. sql.VarChar(60));
            tbl_nb_spaj_skpr.columns.add('jenis_kelamin'. sql.VarChar(10));
            tbl_nb_spaj_skpr.columns.add('tempat_lahir'. sql.VarChar(60));
            tbl_nb_spaj_skpr.columns.add('tgl_lahir'. sql.Date);
            tbl_nb_spaj_skpr.columns.add('id_hubungan'. sql.NVarChar(30));
            tbl_nb_spaj_skpr.columns.add('persentase'. sql.Float);
            tbl_nb_spaj_skpr.columns.add('no_urut'. sql.Int);
            tbl_nb_spaj_skpr.columns.add('kd_martial_status'. sql.VarChar(5));
            tbl_nb_spaj_skpr.columns.add('no_identitas'. sql.VarChar(50));

            //dbo.nb_spaj_uker
            var nb_spaj_uker = req.body.data.nb_spaj_uker;
            var tbl_nb_spaj_uker = new sql.Table();
            tbl_nb_spaj_uker.columns.add('KODE_KANWIL'. sql.VarChar(5));
            tbl_nb_spaj_uker.columns.add('UNIT_KERJA'. sql.Char(1));
            tbl_nb_spaj_uker.columns.add('KODE_CABANG_INDUK'. sql.VarChar(5));
            tbl_nb_spaj_uker.columns.add('SUBBR'. sql.VarChar(5));
            tbl_nb_spaj_uker.columns.add('SBDESC'. sql.VarChar(100));

            //dbo.nb_spaj_underwriter
            var nb_spaj_underwriter = req.body.data.nb_spaj_underwriter;
            var tbl_nb_spaj_underwriter = new sql.Table();
            tbl_nb_spaj_underwriter.columns.add('Transno'. sql.VarChar(20));
            tbl_nb_spaj_underwriter.columns.add('status_udw'. sql.VarChar(50));
            tbl_nb_spaj_underwriter.columns.add('ket_udw'. sql.VarChar);
            tbl_nb_spaj_underwriter.columns.add('user_record'. sql.VarChar(50));
            tbl_nb_spaj_underwriter.columns.add('tgl_record'. sql.DateTime);
            tbl_nb_spaj_underwriter.columns.add('user_update'. sql.VarChar(50));
            tbl_nb_spaj_underwriter.columns.add('tgl_update'. sql.DateTime);

            //dbo.nb_nasabah
            var nb_nasabah = req.body.data.nb_nasabah;
            var tbl_nb_nasabah = new sql.Table();
            tbl_nb_nasabah.columns.add('id_nasabah'. sql.Numeric(18));
            tbl_nb_nasabah.columns.add('CIF'. sql.VarChar(20));
            tbl_nb_nasabah.columns.add('nama_lengkap'. sql.VarChar(120));
            tbl_nb_nasabah.columns.add('gelar_depan'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('gelar_belakang'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('jenis_identitas'. sql.VarChar(25));
            tbl_nb_nasabah.columns.add('no_kartu_identitas'. sql.VarChar(35));
            tbl_nb_nasabah.columns.add('tgl_kadaluarsa'. sql.SmallDateTime);
            tbl_nb_nasabah.columns.add('tempat_lahir'. sql.VarChar(100));
            tbl_nb_nasabah.columns.add('tanggal_lahir'. sql.SmallDateTime);
            tbl_nb_nasabah.columns.add('kewarganegaraan'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('negara'. sql.VarChar(80));
            tbl_nb_nasabah.columns.add('jenis_kelamin'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('agama'. sql.VarChar(20));
            tbl_nb_nasabah.columns.add('agama_lain'. sql.VarChar(20));
            tbl_nb_nasabah.columns.add('status_perkawinan'. sql.VarChar(15));
            tbl_nb_nasabah.columns.add('nama_ibu'. sql.VarChar(120));
            tbl_nb_nasabah.columns.add('alamat_identitas'. sql.VarChar(150));
            tbl_nb_nasabah.columns.add('rt_id'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('rw_id'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('kelurahan_id'. sql.VarChar(80));
            tbl_nb_nasabah.columns.add('kecamatan_id'. sql.VarChar(80));
            tbl_nb_nasabah.columns.add('kota_id'. sql.Int);
            tbl_nb_nasabah.columns.add('propinsi_id'. sql.VarChar(80));
            tbl_nb_nasabah.columns.add('kode_pos_id'. sql.VarChar(6));
            tbl_nb_nasabah.columns.add('alamat_domisili'. sql.VarChar(150));
            tbl_nb_nasabah.columns.add('rt_do'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('rw_do'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('kelurahan_do'. sql.VarChar(80));
            tbl_nb_nasabah.columns.add('kecamatan_do'. sql.VarChar(80));
            tbl_nb_nasabah.columns.add('kota_do'. sql.Int);
            tbl_nb_nasabah.columns.add('propinsi_do'. sql.Int);
            tbl_nb_nasabah.columns.add('kode_pos_do'. sql.VarChar(6));
            tbl_nb_nasabah.columns.add('no_telepon'. sql.NVarChar(20));
            tbl_nb_nasabah.columns.add('no_hp'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('no_fax'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('email_do'. sql.VarChar(100));
            tbl_nb_nasabah.columns.add('id_pendidikan'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('pendidikan_lainnya'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('id_pekerjaan'. sql.VarChar(30));
            tbl_nb_nasabah.columns.add('pekerjaan_lainnya'. sql.VarChar(120));
            tbl_nb_nasabah.columns.add('pekerjaan_lainnya_dv'. sql.VarChar(120));
            tbl_nb_nasabah.columns.add('id_bidang_usaha'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('id_jabatan'. sql.VarChar(10));
            tbl_nb_nasabah.columns.add('jabatan_lainnya'. sql.VarChar(120));
            tbl_nb_nasabah.columns.add('nama_institusi'. sql.VarChar(150));
            tbl_nb_nasabah.columns.add('alamat_institusi'. sql.VarChar(150));
            tbl_nb_nasabah.columns.add('rt_ins'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('rw_ins'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('kelurahan_ins'. sql.VarChar(60));
            tbl_nb_nasabah.columns.add('kecamatan_ins'. sql.VarChar(60));
            tbl_nb_nasabah.columns.add('kota_ins'. sql.Int);
            tbl_nb_nasabah.columns.add('propinsi_ins'. sql.Int);
            tbl_nb_nasabah.columns.add('kode_pos_ins'. sql.VarChar(6));
            tbl_nb_nasabah.columns.add('telepon_kantor'. sql.VarChar(20));
            tbl_nb_nasabah.columns.add('id_alamat_surat'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('npwp'. sql.VarChar(30));
            tbl_nb_nasabah.columns.add('id_pengiriman_polis'. sql.VarChar(50));
            tbl_nb_nasabah.columns.add('SCIF'. sql.VarChar);

            //dbo.nb_nasabah_bypremi
            var nb_nasabah_bypremi = req.body.data.nb_nasabah_bypremi;
            var tbl_nb_nasabah_bypremi = new sql.Table();
            tbl_nb_nasabah_bypremi.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_nasabah_bypremi.columns.add('id_nasabah'. sql.Numeric(18));
            tbl_nb_nasabah_bypremi.columns.add('premijawaban1'. sql.Int);
            tbl_nb_nasabah_bypremi.columns.add('tujuanAsuransi'. sql.VarChar);
            tbl_nb_nasabah_bypremi.columns.add('ketTujuanAsuransi'. sql.NVarChar);
            tbl_nb_nasabah_bypremi.columns.add('sumberPenghasilan'. sql.NVarChar);
            tbl_nb_nasabah_bypremi.columns.add('ketSumberPenghasilan'. sql.NVarChar(80));
            tbl_nb_nasabah_bypremi.columns.add('jmlPenghasilan'. sql.Int);
            tbl_nb_nasabah_bypremi.columns.add('caraPembayaranPremi'. sql.VarChar(50));
            tbl_nb_nasabah_bypremi.columns.add('periodeBayar'. sql.VarChar(50));
            tbl_nb_nasabah_bypremi.columns.add('masaByrPremi'. sql.Int);
            tbl_nb_nasabah_bypremi.columns.add('caraPembayaranManfaat'. sql.VarChar(50));

            //dbo.nb_pelunasan
            var nb_pelunasan = req.body.data.nb_pelunasan;
            var tbl_nb_pelunasan = new sql.Table();
            tbl_nb_pelunasan.columns.add('id'. sql.Int);
            tbl_nb_pelunasan.columns.add('transno'. sql.VarChar(18));
            tbl_nb_pelunasan.columns.add('cara_bayar'. sql.Int);
            tbl_nb_pelunasan.columns.add('rekening'. sql.VarChar(30));
            tbl_nb_pelunasan.columns.add('tgl'. sql.NVarChar);
            tbl_nb_pelunasan.columns.add('sumberPenghasilan'. sql.DateTime);
            tbl_nb_pelunasan.columns.add('tgl_tagihan'. sql.Decimal(18));
            tbl_nb_pelunasan.columns.add('tgl_jatuh_tempo'. sql.DateTime);
            tbl_nb_pelunasan.columns.add('total_nasabah_tagihan'. sql.Int);
            tbl_nb_pelunasan.columns.add('jml_rec_tagihan'. sql.Int);
            tbl_nb_pelunasan.columns.add('tgl_pelunasan'. sql.DateTime);
            tbl_nb_pelunasan.columns.add('total_pelunasan'. sql.Decimal(18));
            tbl_nb_pelunasan.columns.add('total_nasabah_pelunasan'. sql.Int);
            tbl_nb_pelunasan.columns.add('jml_rec_pelunasan'. sql.Int);

            //dbo.nb_pelunasan_detail
            var nb_pelunasan_detail = req.body.data.nb_pelunasan_detail;
            var tbl_nb_pelunasan_detail = new sql.Table();
            tbl_nb_pelunasan_detail.columns.add('no'. sql.Numeric(18));
            tbl_nb_pelunasan_detail.columns.add('id_pelunasan'. sql.Int);
            tbl_nb_pelunasan_detail.columns.add('pp_pl'. sql.Char(2));
            tbl_nb_pelunasan_detail.columns.add('bank'. sql.VarChar(50));
            tbl_nb_pelunasan_detail.columns.add('rekening'. sql.VarChar(20));
            tbl_nb_pelunasan_detail.columns.add('nama_nasabah'. sql.VarChar(120));
            tbl_nb_pelunasan_detail.columns.add('amount'. sql.Float);
            tbl_nb_pelunasan_detail.columns.add('charge'. sql.Float);
            tbl_nb_pelunasan_detail.columns.add('remaks'. sql.VarChar(120));
            tbl_nb_pelunasan_detail.columns.add('tagihan_ke'. sql.Int);
            tbl_nb_pelunasan_detail.columns.add('tgl_penagihan'. sql.Date);
            tbl_nb_pelunasan_detail.columns.add('kd_status'. sql.VarChar(5));
            tbl_nb_pelunasan_detail.columns.add('due_date'. sql.Date);
            tbl_nb_pelunasan_detail.columns.add('status'. sql.VarChar(150));
            tbl_nb_pelunasan_detail.columns.add('tgl_bayar'. sql.Date);
            tbl_nb_pelunasan_detail.columns.add('reason'. sql.VarChar(150));
            tbl_nb_pelunasan_detail.columns.add('verifikasi_nonBRI'. sql.TinyInt);
            tbl_nb_pelunasan_detail.columns.add('trans_no'. sql.VarChar(18));
            tbl_nb_pelunasan_detail.columns.add('status_sms_terbayar'. sql.Int);
            tbl_nb_pelunasan_detail.columns.add('tagihan_briva'. sql.Int);
            tbl_nb_pelunasan_detail.columns.add('status_gen_pl'. sql.Int);

            //dbo.nb_pelunasan_file
            var nb_pelunasan_file = req.body.data.nb_pelunasan_file;
            var tbl_nb_pelunasan_file = new sql.Table();
            tbl_nb_pelunasan_file.columns.add('id'. sql.Int);
            tbl_nb_pelunasan_file.columns.add('transno'. sql.VarChar(18));
            tbl_nb_pelunasan_file.columns.add('id_tipe_file'. sql.Int);
            tbl_nb_pelunasan_file.columns.add('keterangan_file'. sql.NVarChar);
            tbl_nb_pelunasan_file.columns.add('content_file'. sql.VarChar(50));
            tbl_nb_pelunasan_file.columns.add('length_file'. sql.Int);
            tbl_nb_pelunasan_file.columns.add('attach_file'. sql.VarBinary);

            //dbo.nb_dsp
            var nb_dsp = req.body.data.nb_dsp;
            var tbl_nb_dsp = new sql.Table();
            tbl_nb_dsp.columns.add('id_dsp'. sql.Numeric(18));
            tbl_nb_dsp.columns.add('id_aplikasi'. sql.Numeric(18));
            tbl_nb_dsp.columns.add('id_tagihan'. sql.SmallInt);
            tbl_nb_dsp.columns.add('status_byr'. sql.SmallInt);
            tbl_nb_dsp.columns.add('mt_uang'. sql.Int);
            tbl_nb_dsp.columns.add('bentuk_byr'. sql.NVarChar(10));
            tbl_nb_dsp.columns.add('no_dsp'. sql.NVarChar(50));
            tbl_nb_dsp.columns.add('giro_bjs'. sql.NVarChar(20));
            tbl_nb_dsp.columns.add('premi'. sql.Float);
            tbl_nb_dsp.columns.add('format_dsp'. sql.NVarChar(200));
            tbl_nb_dsp.columns.add('is_validasi'. sql.Int);
            tbl_nb_dsp.columns.add('tglvalidasi'. sql.DateTime);
            tbl_nb_dsp.columns.add('uservalidasi'. sql.NVarChar(50));
            tbl_nb_dsp.columns.add('tgltransfer'. sql.DateTime);
            tbl_nb_dsp.columns.add('status_valid'. sql.VarChar(50));

            //dbo.nb_dsp_detail
            var nb_dsp_detail = req.body.data.nb_dsp_detail;
            var tbl_nb_dsp_detail = new sql.Table();
            tbl_nb_dsp_detail.columns.add('iddetil'. sql.Numeric(18));
            tbl_nb_dsp_detail.columns.add('id_dsp'. sql.Numeric(18));
            tbl_nb_dsp_detail.columns.add('termin'. sql.Int);
            tbl_nb_dsp_detail.columns.add('id_bntbayar'. sql.Int);
            tbl_nb_dsp_detail.columns.add('discount'. sql.Float);
            tbl_nb_dsp_detail.columns.add('bayar'. sql.Float);
            tbl_nb_dsp_detail.columns.add('selisih'. sql.Float);
            tbl_nb_dsp_detail.columns.add('materai'. sql.Float);
            tbl_nb_dsp_detail.columns.add('keterangan'. sql.NVarChar);
            tbl_nb_dsp_detail.columns.add('bayar_premi_dasar'. sql.Float);
            tbl_nb_dsp_detail.columns.add('bayar_premi_topup'. sql.Float);
            tbl_nb_dsp_detail.columns.add('bayar_premi_topup_tunggal'. sql.Float);
            
            //dbo.nb_dsp_integral
            var nb_dsp_integral = req.body.data.nb_dsp_integral;
            var tbl_nb_dsp_integral = new sql.Table();
            tbl_nb_dsp_detail.columns.add('id'. sql.Int);
            tbl_nb_dsp_detail.columns.add('no_spaj'. sql.VarChar(50));
            tbl_nb_dsp_detail.columns.add('ILContractNumber'. sql.VarChar(50));
            tbl_nb_dsp_detail.columns.add('pembayaran_ke'. sql.Int);
            tbl_nb_dsp_detail.columns.add('PaymentMethod'. sql.VarChar(50));
            tbl_nb_dsp_detail.columns.add('BankCode'. sql.VarChar(50));
            tbl_nb_dsp_detail.columns.add('Amount'. sql.VarChar(500));
            tbl_nb_dsp_detail.columns.add('OtherReferenceNumber'. sql.VarChar(50));
            tbl_nb_dsp_detail.columns.add('keteremarkrangan'. sql.VarChar(100));
            tbl_nb_dsp_detail.columns.add('date'. sql.DateTime);


            //dbo.nb_spaj
                for(var a in nb_spaj){
                    //jika kolom dan data beda
                    // let tmp = {
                    //     id_aplikasi : nb_spaj[a].id_aplikasi,
                    //     TransNo : nb_spaj[a].TransNo,
                    // }
                    // tbl_nbspaj.rows.add(tmp);

                    //jika colom dan data sama
                    tbl_nbspaj.rows.add(nb_spaj[a]);
                };

            //nb_spaj_agen
                for(var a in nb_spaj_agen){
                    tbl_nb_spaj_agen.rows.add(nb_spaj_agen[a]);
                };

            //nb_spaj_biller
                for(var a in nb_spaj_biller){
                    tbl_nb_spaj_biller.rows.add(nb_spaj_biller[a]);
                };

            //nb_spaj_biller_proses
                for(var a in nb_spaj_biller_proses){
                    tbl_nb_spaj_biller_proses.rows.add(nb_spaj_biller_proses[a]);
                };

            //nb_spaj_file_biller
                for(var a in nb_spaj_file_biller){
                    tbl_nb_spaj_file_biller.rows.add(nb_spaj_file_biller[a]);
                };

            //nb_spaj_history_underwriter
                for(var a in nb_spaj_history_underwriter){
                    tbl_nb_spaj_history_underwriter.rows.add(nb_spaj_history_underwriter[a]);
                };

            //nb_spaj_integral
                for(var a in nb_spaj_integral){
                    tbl_nb_spaj_integral.rows.add(nb_spaj_integral[a]);
                };

            //nb_spaj_integral_hist
                for(var a in nb_spaj_integral_hist){
                    tbl_nb_spaj_integral_hist.rows.add(nb_spaj_integral_hist[a]);
                };

            //nb_spaj_kuesion_medis
                for(var a in nb_spaj_kuesion_medis){
                    tbl_nb_spaj_kuesion_medis.rows.add(nb_spaj_kuesion_medis[a]);
                };

            //nb_spaj_master
                for(var a in nb_spaj_master){
                    tbl_nb_spaj_master.rows.add(nb_spaj_master[a]);
                };

            //nb_spaj_master_map$
                for(var a in nb_spaj_master_map$){
                    tbl_nb_spaj_master_map$.rows.add(nb_spaj_master_map$[a]);
                };

            //nb_spaj_pemilikanasr
                for(var a in nb_spaj_pemilikanasr){
                    tbl_nb_spaj_pemilikanasr.rows.add(nb_spaj_pemilikanasr[a]);
                };

            //nb_spaj_pemilikanasr_ket_2
                for(var a in nb_spaj_pemilikanasr_ket_2){
                    tbl_nb_spaj_pemilikanasr_ket_2.rows.add(nb_spaj_pemilikanasr_ket_2[a]);
                };

            //nb_spaj_pemilikanasr_ket_3
                for(var a in nb_spaj_pemilikanasr_ket_3){
                    tbl_nb_spaj_pemilikanasr_ket_3.rows.add(nb_spaj_pemilikanasr_ket_3[a]);
                };

            //nb_spaj_pert_investasi
                for(var a in nb_spaj_pert_investasi){
                    tbl_nb_spaj_pert_investasi.rows.add(nb_spaj_pert_investasi[a]);
                };

            //nb_spaj_profil_resiko
                for(var a in nb_spaj_profil_resiko){
                    tbl_nb_spaj_profil_resiko.rows.add(nb_spaj_profil_resiko[a]);
                };

            //nb_spaj_refferal
                for(var a in nb_spaj_refferal){
                    tbl_nb_spaj_refferal.rows.add(nb_spaj_refferal[a]);
                };

            //nb_spaj_row_invest_pert_investasi
                for(var a in nb_spaj_row_invest_pert_investasi){
                    tbl_nb_spaj_row_invest_pert_investasi.rows.add(nb_spaj_row_invest_pert_investasi[a]);
                };

            //nb_spaj_kuesion_medis
                for(var a in nb_spaj_row_rider_pert_investasi){
                    tbl_nb_spaj_row_rider_pert_investasi.rows.add(nb_spaj_row_rider_pert_investasi[a]);
                };

            //nb_spaj_master
                for(var a in nb_spaj_setup){
                    tbl_nb_spaj_setup.rows.add(nb_spaj_setup[a]);
                };

            //nb_spaj_skpr
                for(var a in nb_spaj_skpr){
                    tbl_nb_spaj_skpr.rows.add(nb_spaj_skpr[a]);
                };

            //nb_spaj_pemilikanasr
                for(var a in nb_spaj_pemilikanasr){
                    tbl_nb_spaj_pemilikanasr.rows.add(nb_spaj_pemilikanasr[a]);
                };

            //nb_spaj_pemilikanasr_ket_2
                for(var a in nb_spaj_pemilikanasr_ket_2){
                    tbl_nb_spaj_pemilikanasr_ket_2.rows.add(nb_spaj_pemilikanasr_ket_2[a]);
                };

            //nb_spaj_pemilikanasr_ket_3
                for(var a in nb_spaj_pemilikanasr_ket_3){
                    tbl_nb_spaj_pemilikanasr_ket_3.rows.add(nb_spaj_pemilikanasr_ket_3[a]);
                };

            //nb_spaj_pert_investasi
                for(var a in nb_spaj_pert_investasi){
                    tbl_nb_spaj_pert_investasi.rows.add(nb_spaj_pert_investasi[a]);
                };

            //nb_spaj_profil_resiko
                for(var a in nb_spaj_profil_resiko){
                    tbl_nb_spaj_profil_resiko.rows.add(nb_spaj_profil_resiko[a]);
                };

            //nb_spaj_refferal
                for(var a in nb_spaj_refferal){
                    tbl_nb_spaj_refferal.rows.add(nb_spaj_refferal[a]);
                };

                
                let spInsert = {
                    conn: 'ganti koneksi',
                    sp: 'butuh sp baru',
                    returnProperti: 'data',
                    params: [
                        {
                            param: 'tbl_nbspaj',
                            type: sql.TVP,
                            value: tbl_nbspaj
                        },
                        {
                            param: 'tbl_nb_spaj_agen',
                            type: sql.TVP,
                            value: tbl_nb_spaj_agen
                        },
                        {
                            param: 'tbl_nb_spaj_biller',
                            type: sql.TVP,
                            value: tbl_nb_spaj_biller
                        },
                        {
                            param: 'tbl_nb_spaj_biller_proses',
                            type: sql.TVP,
                            value: tbl_nb_spaj_biller_proses
                        }
                        ,{
                            param: 'tbl_nb_spaj_file_biller',
                            type: sql.TVP,
                            value: tbl_nb_spaj_file_biller
                        },
                        {
                            param: 'tbl_nb_spaj_integral',
                            type: sql.TVP,
                            value: tbl_nb_spaj_integral
                        },
                        {
                            param: 'tbl_nb_spaj_integral_hist',
                            type: sql.TVP,
                            value: tbl_nb_spaj_integral_hist
                        },
                        {
                            param: 'tbl_nb_spaj_attach',
                            type: sql.TVP,
                            value: tbl_nb_spaj_attach
                        },
                        {
                            param: 'tbl_nb_spaj_history_underwriter',
                            type: sql.TVP,
                            value: tbl_nb_spaj_history_underwriter
                        },
                        {
                            param: 'tbl_nb_spaj_kuesion_medis',
                            type: sql.TVP,
                            value: tbl_nb_spaj_kuesion_medis
                        },
                        {
                            param: 'tbl_nb_spaj_master',
                            type: sql.TVP,
                            value: tbl_nb_spaj_master
                        }
                        ,{
                            param: 'tbl_nb_spaj_master_map$',
                            type: sql.TVP,
                            value: tbl_nb_spaj_master_map$
                        },
                        {
                            param: 'tbl_nb_spaj_pemilikanasr',
                            type: sql.TVP,
                            value: tbl_nb_spaj_pemilikanasr
                        },
                        {
                            param: 'tbl_nb_spaj_pemilikanasr_ket_2',
                            type: sql.TVP,
                            value: tbl_nb_spaj_pemilikanasr_ket_2
                        },
                        {
                            param: 'tbl_nb_spaj_pemilikanasr_ket_3',
                            type: sql.TVP,
                            value: tbl_nb_spaj_pemilikanasr_ket_3
                        },
                        {
                            param: 'tbl_nb_spaj_pert_investasi',
                            type: sql.TVP,
                            value: tbl_nb_spaj_pert_investasi
                        },
                        {
                            param: 'tbl_nb_spaj_profil_resiko',
                            type: sql.TVP,
                            value: tbl_nb_spaj_profil_resiko
                        },
                        {
                            param: 'tbl_nb_spaj_refferal',
                            type: sql.TVP,
                            value: tbl_nb_spaj_refferal
                        }
                        ,{
                            param: 'tbl_nb_spaj_row_invest_pert_investasi',
                            type: sql.TVP,
                            value: tbl_nb_spaj_row_invest_pert_investasi
                        },
                        {
                            param: 'tbl_nb_spaj_row_rider_pert_investasi',
                            type: sql.TVP,
                            value: tbl_nb_spaj_row_rider_pert_investasi
                        },
                        {
                            param: 'tbl_nb_spaj_setup',
                            type: sql.TVP,
                            value: tbl_nb_spaj_setup
                        },
                        {
                            param: 'tbl_nb_spaj_skpr',
                            type: sql.TVP,
                            value: tbl_nb_spaj_skpr
                        },
                        {
                            param: 'tbl_nb_spaj_termaslahat',
                            type: sql.TVP,
                            value: tbl_nb_spaj_termaslahat
                        },
                        {
                            param: 'tbl_nb_spaj_uker',
                            type: sql.TVP,
                            value: tbl_nb_spaj_uker
                        },
                        {
                            param: 'tbl_nb_spaj_underwriter',
                            type: sql.TVP,
                            value: tbl_nb_spaj_underwriter
                        }
                        ,{
                            param: 'tbl_nb_nasabah',
                            type: sql.TVP,
                            value: tbl_nb_nasabah
                        },
                        {
                            param: 'tbl_nb_nasabah_bypremi',
                            type: sql.TVP,
                            value: tbl_nb_nasabah_bypremi
                        },
                        {
                            param: 'tbl_nb_pelunasan',
                            type: sql.TVP,
                            value: tbl_nb_pelunasan
                        },
                        {
                            param: 'tbl_nb_pelunasan_detail',
                            type: sql.TVP,
                            value: tbl_nb_pelunasan_detail
                        },
                        {
                            param: 'tbl_nb_pelunasan_file',
                            type: sql.TVP,
                            value: tbl_nb_pelunasan_file
                        },
                        {
                            param: 'tbl_nb_dsp',
                            type: sql.TVP,
                            value: tbl_nb_dsp
                        }
                        ,{
                            param: 'tbl_nb_dsp_detail',
                            type: sql.TVP,
                            value: tbl_nb_dsp_detail
                        },
                        {
                            param: 'tbl_nb_dsp_integral',
                            type: sql.TVP,
                            value: tbl_nb_dsp_integral
                        }

                    ]
                }

                var hasil_cp = await helpers.exec_sp(spInsert);

                reshasil=hasil_cp;
                if(reshasil.rescode == '00'){
                    if(reshasil.resdata.length > 0){
                        let ress = {
                            success: 'ok',
                            data:  reshasil.resdata
                        }
                        
                        res.send(ress);
                    }else{
                        let ress = {
                            status : 'error',
                            error : 'Data tidak ditemukan',
                        }
                        res.send(ress);
                    }
                }else{
                    let ress = {
                        status : 'error',
                        error : reshasil.reserror? reshasil.reserror[0].errmsg : '',
                    }

                    res.send(ress);
                }

            }catch (err) {
                let ress = {
                    status : 'error',
                    error : err ? (err.message ? err.message : '') : '',
                }
                res.send(ress)
                return;
            }
            
        }
    